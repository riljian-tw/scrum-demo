<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class UtilsTest extends TestCase
{
    public function testGetIndexToast(): void
    {
        require 'src/utils/get_toasts.php';
        $this->assertStringStartsWith(
            'Hello, this is version',
            get_index_toast()
        );
    }
}

