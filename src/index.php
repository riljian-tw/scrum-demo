<?php
/**
 * Index
 *
 * PHP version 8
 *
 * @category Scrum_Demo
 * @package  Scrum_Demo
 * @author   Jamie Lin <riljian.tw@gmail.com>
 * @license  https://public.license/ Public Licence
 * @link     -
 */

require './utils/get_toasts.php';

?>
<html>
<head>
    <title>Hello world</title>
</head>
<body>
    <h1><?php echo get_index_toast(); ?></h1>
    <h2><?php echo 'environment: '.getenv('ENV_NAME'); ?></h2>
    <label>Feature A</label>
</body>
</html>
