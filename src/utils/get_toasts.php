<?php
/**
 * Get toasts
 *
 * PHP version 8
 *
 * @category Scrum_Demo
 * @package  Scrum_Demo
 * @author   Jamie Lin <riljian.tw@gmail.com>
 * @license  https://public.license/ Public Licence
 * @link     -
 */

 /**
  * Get index toast
  *
  * @return string
  **/
function Get_Index_toast()
{
    return 'Hello, this is version '.getenv('COMMIT_SHA');
}

?>
